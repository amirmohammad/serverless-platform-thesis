// Global Dependencies
require('dotenv').config()
require('app-module-path').addPath(__dirname)

// Local Files Dependencies
global.config = require('./src/configs')

const App = require('./src')

new App();