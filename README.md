# Cold Start Mitigation Using LightVMs
This title is my thesis in Shahid Beheshti University of Tehran, Iran.

<br/>

## Application Architecture

[Architecture to add]

<br/>

## components and services
[Components and services]

## How to Run Project?

<br/>


## PreRequirements

- Nodejs (project run on 18.3v)
- Mongodb Server
- Docker
- Kafka Messaging (can be set up via docker-compose, Compose file is avialable in the repo)
- python 3.x (Mine was 3.9.2)
- gvisor installed and configured with docker (see [gvisor with docker](https://gvisor.dev/docs/user_guide/quick_start/docker/).)



## Setup Nodejs server

run `npm install` and installed desired packages in man route of dicrectory.

run `npm start` to start the server on desired port.

api request routes are in `thunder-collection_serverless_thesis.json` . 

<br/> 

## setup Message Broker

We've used apache kafka as the message broker. In this project we have setup kafka cluster with docker-compose.
to set up the cluster, just go to `src/services/message-broker`, then run `docker-compose up -d --build` to have a new cluster. 


<br/> 

## setup metric collection service

route to `src/services/logsToDB`

setup a virtualenvironment with `python3 -m venv venv`. then setup venv with `source ./venv/bin/activate`.

In the next step, you should install dependencies. type `pip install -r requirements.txt` to install dependencies.

Change MongoClient information if needed (Default is `mongodb://127.0.0.1:27017`).

run `python3 consumer.py`. Your service is up and running. 

In order to create topics in Kafka Client, you can run `create-topic.py`.

<br/>

## export dataset from database to csv file + backup database info

check `src/services/db to dataset` in this project. run `dbToDataset.sh` to export dataset to `dataset.csv` file.

You can have a backup of your database running `wholeDBExport.sh`. output will be in `db` directory placed in current directory. 

<br/>

## ZeroScaling service 

route to `src/services/zero_scale`.
 setup virtual env by typing `python -m venv venv`. 
 
 Then install required packages by typing `pip install -r requriements.txt`. 

 Zeroscaler service is a cronjob running by linux `crontab` service. to run the crontab, you should just run `job.sh` file. to change zeroscaling duration, change `warm_duration_in_minutes` in `zeroScaler.py`. 
