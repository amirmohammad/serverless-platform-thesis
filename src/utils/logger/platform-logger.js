const { format, createLogger, transports } = require('winston');
const { timestamp, combine, printf, errors, json } = format;

function buildDevLogger() {
    const logFormat = printf(({ level, message, timestamp, stack }) => {
      return `${timestamp} ${level}: ${stack || message}`;
    });

    const myCustomLevels = {
      levels: {
        emerg: 0,
        alert: 1,
        crit: 2,
        error: 3,
        warning: 4,
        notice: 5,
        info: 6,
        debug: 7
      },
      colors: {
        emerg: 'red',
        alert: 'orange',
        crit: 'orange',
        erorr: 'red',
        warning: 'yellow',
        notice: 'green',
        info: 'blue',
        debug: 'blue'
      }
    };
  
    return createLogger({
      levels: myCustomLevels.levels, 
      format: combine(
        format.colorize(),
        timestamp({ format: 'YYYY-MM-DD HH:mm:ss:SSSS' }),
        errors({ stack: true }),
        json(),
        logFormat
      ),
      transports: [
        new transports.Console(),
        new transports.File({ filename: 'Errors.log', level: 'error' }),
        new transports.File({ filename: 'Info.log', level: 'info' }),
        new transports.File({ filename: 'Notice.log', level: 'notice' }),
      ],
      exceptionHandlers: [
        new transports.File({ filename: 'Exceptions.log' }),
      ],
      rejectionHandlers: [
        new transports.File({ filename: 'Rejections.log' })
      ]
    });
  }
  
  module.exports = buildDevLogger;
