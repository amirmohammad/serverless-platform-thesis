
const AllInvocedFunctionsTransform = (functions) => {

    if (functions.length == 0) return [];

    let result = []

    for (let func of functions) {
        let obj = {
            "Name": func.Name,
            "ImageID": func.ImageID, 
            "Runtime": func.Runtime, 
        }
        result.push(obj)
    }

    return result

}

module.exports = {
    AllInvocedFunctionsTransform, 
}