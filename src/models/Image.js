const mongoose = require('mongoose')
const uniqueString = require('unique-string');

const InvocationSchema = new mongoose.Schema({
    InvocationID: {type: String, default: uniqueString()}
}, {timestamps: true})

const ImageSchema = new mongoose.Schema({
    ImageID: {type: String, required: true, default: uniqueString(), unique: true},
    Runtime: {type: String,},
    Name: {type: String, required: true}, 
    Application: {type: String, required: true}, 
    Inovcations: [InvocationSchema]
}, {timestamps: true, toJSON: {virtuals: true}}); 

// module.exports.Invocation = mongoose.model('Invocation', InvocationSchema); 
module.exports = mongoose.model('Image', ImageSchema); 