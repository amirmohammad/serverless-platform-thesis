
const autoBind = require('auto-bind');
const { validationResult } = require('express-validator')


class controller {

    constructor () {
        autoBind(this)
    }

    async responseMesaage(req, res, statusCode, status, imageName, command, route, message) {

        let isSuccess = (await statusCode == 200) ? true : false; 

        return res.status(Number(statusCode)).json( {
            "Status": status,
            "Code": statusCode,
            "Success" : isSuccess,
            "Data": {
                "Image": imageName, 
                "Command": command,
                "Route": route,
                "Message": message,
                "timestamp": Date.now()
            }
            
        })
    }

    async responseMesaageWithData(req, res, statusCode, status, imageName, command, route, message, data) {
        let isSuccess = (await statusCode == 200) ? true : false; 

        return res.status(Number(statusCode)).json( {
            "Status": status,
            "Code": statusCode,
            "Success" : isSuccess,
            "Data": {
                "Image": imageName, 
                "Command": command,
                "Route": route,
                "Message": message,
                "timestamp": Date.now(), 
                ...data
            }
            
        })
    }

    responseFunctionExecution(res, statusCode, status, container, image, message, data, ExecutionTime) {
        return res.status(200).json({
            "Status": status,
            "Code": statusCode,
            "Container": container, 
            "Image": image,
            "Message": message,
            "ExecutionTime": ExecutionTime,
            "Data": {...data}
            
        })
    }

    async validationData(req , res) {
        const result = validationResult(req);
        if (! result.isEmpty()) {
            const errors = result.array();
            const messages = [];
           
            await errors.forEach(err => messages.push(err.msg));
            // this.failed(messages , res , 422);
            this.responseMesaageWithData(req, res, 422, 'Failed', '', 'deploy image', '/deploy', 'Invalid Inputs', {messages: [messages]})
            // return res.json(messages)

            return false;
        }

        return true;
    }


}

module.exports = controller; 