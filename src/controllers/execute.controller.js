
// 3rd party dependencies
const Docker = require('dockerode')
const docker = new Docker(); 
const {spawn} = require('child_process')
const uniqueString = require('unique-string');
const bent = require('bent')
const getJson = bent('json')
const randomstring = require("randomstring");
const moment = require('moment')

// local dependencies
const controller = require('./index.controllers');
const ImageSchema = require('../models/Image');
const logger = require('../utils/logger')
const producer = require('../services/message-broker/producer')
const deployController = require('./deploy.controller')

// kafka
const {Kafka} = require('kafkajs')

class executeControllers extends controller {

    async execute(req, res, next) {
        if(! await this.validationData(req, res)) return;

        const {runtime, functionBody } = req.body;
        const { user, func } = req.params;        

        logger.info(`${user}/${func} runtime: ${runtime}`)

        const date1 = new Date()
        
        // const {id} = req.params; 
        const id = `${user}/${func}:latest` 
        // const invocationTS = new Date(); 
        const invocationTS = moment().format('YYYY-MM-DD HH:mm:ss'); 
        logger.info(`${id} Starts Executing`)
        logger.notice(`Image: ${id}`) 

        try {
            const image = await ImageSchema.findOne({"Name": id})
            
            if(!image) {
                logger.notice(`Image ${id} Not Found in the db`)
                return this.responseMesaage(req, res, 404, 'Failed', id, '', '/execution', 'Image not found :((')
            }
            //image found, check for runtime to run image
            if(runtime == 'runsc'){
                logger.info(`${id} is Running in gvisor block`)
                
                // check if container has a running instance
                const imageHasInstance = await this.haveInstance(image.Name, runtime)
                
                if (imageHasInstance != false) {
                    logger.info(`${id} image has instance`)
                    const date2 = new Date()
                    const executionTime = date2 - date1;
                    const functionExecResult = await this.callApi(imageHasInstance.port, functionBody)
                    logger.notice(`${id}: WarmStart`)

                    this.logInfo(invocationTS, id, imageHasInstance.id, false, image.Application, executionTime, "runsc")
                    return this.responseFunctionExecution(res, 200, 'Success', '', '', 'WarmStart', functionExecResult, executionTime)
                }

                let result = await this.runGvisorFunction(image.Name)

                if (result == false) {
                    logger.error(result)
                    return this.responseMesaage(req, res, 500, 'Failed', '', '', '/execution', 'Execution error. check log files')
                }
                else {
                    // By experiense, 3500 milliseconds is the optimal duration to make the contianer fully up. 
                    // await this.sleep(global.config.docker.containerWaitOptimalDuration)
                    const {containerId, containerPort} = result;
                    
                    const functionExecResult = await this.callApi(containerPort, functionBody);
                    
                    const date2 = new Date()
                    const executionTime = date2 - date1;
                    logger.notice(`${id} ColdStart`)
                    this.logInfo(invocationTS, id, containerId, true, image.Application, executionTime, "runsc")
                    return this.responseFunctionExecution(res, 200, 'Success', containerId, '', 'ColdStart', functionExecResult, executionTime)
                }
            }
            else if (runtime == 'runc') {
                logger.info('inside container block')
                console.log(image.Name, runtime)
                const imageHasInstance = await this.haveInstance(image.Name, runtime)
                // return res.json(imageHasInstance)
                if (imageHasInstance != false) {
                    logger.info(`${id} image has instance`)
                    const date2 = new Date()
                    const executionTime = date2 - date1;
                    const functionExecResult = await this.callApi(imageHasInstance.port, functionBody)
                    logger.notice(`${id}: WarmStart`)

                    this.logInfo(invocationTS, id, imageHasInstance.id, false, image.Application, executionTime, "runc")
                    return this.responseFunctionExecution(res, 200, 'Success', '', '', 'WarmStart', functionExecResult, executionTime)
                } 
                
                let result = await this.runContainerFunction(image.Name)

                if (result == false) {
                    logger.error(result)
                    return this.responseMesaage(req, res, 500, 'Failed', '', '', '/execution', 'Execution error. check log files')
                }
                else {
                    // By experiense, 3500 milliseconds is the optimal duration to make the contianer fully up. 
                    // await this.sleep(global.config.docker.containerWaitOptimalDuration)
                    const {containerId, containerPort} = result;
                    
                    const functionExecResult = await this.callApi(containerPort, functionBody);
                    
                    const date2 = new Date()
                    const executionTime = date2 - date1;
                    logger.notice(`${id} ColdStart`)
                    this.logInfo(invocationTS, id, containerId, true, image.Application, executionTime, "runsc")
                    return this.responseFunctionExecution(res, 200, 'Success', containerId, '', 'ColdStart', functionExecResult, executionTime)
                }

            }
            else {
                return this.responseMesaage(req, res, 302, 'Failed', '', '', '/execution', 'only runc(container) and runsc(gvisor) runtimes are supported at this time')
            }
            
        } catch (error) {
            logger.error(error)
            return this.responseMesaage(req, res, 500, 'Failed', '', '', '/execution', 'Server-Side Error')
        } finally {
            
        }
    }

    async runGvisorFunction(ImageName) {
        try {
            const containerName = await randomstring.generate(10);
            logger.notice(`${ImageName}: ${containerName}`)
            // const dockerRun = await spawn('docker', ['run', '--rm', '-P', '-d', '--runtime=runsc', `--network=${global.config.docker.bridge}`, `--name=${containerName}`, `${ImageName}`])
            // await this.sleep(global.config.docker.containerWaitOptimalDuration)

            let container = await docker.createContainer({
                "name": containerName,
                "Image": ImageName,
                "ExposedPorts": {"3000/tcp": {}},
                "HostConfig": {
                    "PortBindings": {
                      "3000/tcp": [
                        {
                          "HostPort": "0"   //Map container to a random unused port.
                        }
                      ]
                    }, 
                    "Runtime": "runsc"
                },
            })

        
            await container.start()
            await this.sleep(4000)

            const containers = await docker.listContainers()

            let containerId = null;
            let containerPort = null

            for (let container of containers) {
                if(container.Names[0] == `/${containerName}`) {
                    containerId = container.Id
                    containerPort = container.Ports[0].PublicPort || null
                }
            }
            return {containerId, containerPort, containerName}

        } catch (error) {
            logger.error(error)
            return false
        }
    }

    async runContainerFunction(ImageName) {
        try {
            const containerName = await randomstring.generate(10);
            logger.notice(`${ImageName}: ${containerName}`)

            let container = await docker.createContainer({
                "name": containerName,
                "Image": ImageName,
                "ExposedPorts": {"3000/tcp": {}},
                "HostConfig": {
                    "PortBindings": {
                      "3000/tcp": [
                        {
                          "HostPort": "0"   //Map container to a random unused port.
                        }
                      ]
                    }
                },
            })

        
            await container.start()
            await this.sleep(3500)

            const containers = await docker.listContainers()

            let containerId = null;
            let containerPort = null

            for (let container of containers) {
                if(container.Names[0] == `/${containerName}`) {
                    containerId = container.Id
                    containerPort = container.Ports[0].PublicPort || null
                }
            }
            return {containerId, containerPort, containerName}
        } catch (error) {
            logger.error(error)
            return false
        }
    }

    sleep(ms) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }

    async callApi(port, functionBody) {
        
        const apipath = `${global.config.host.communicationProtocol}://${global.config.host.url}:${port}/`;
        const api = await getJson(apipath, functionBody)
        
        return api
        // return true
    }

    async haveInstance(image, runtime) {
        const containers = await docker.listContainers()
        
        if (containers.length == 0) {
            return false;
        }
        
        for (let container of containers) {
            if (container.Image == image) {
                // check for runtime
                let info = await docker.getContainer(container.Id)
                info = await info.inspect()
                let containerRuntime = info.HostConfig.Runtime
                console.log(image, containerRuntime)
                if (containerRuntime == runtime) {
                    const containerPort = container.Ports[0].PublicPort
                    return {port: containerPort, id: container.Id}
                }
                
            }
        }

        return false;

    }

    async logInfo(timestamp, image, container, coldstart, application, executionTime, runtime){

        const info =  {
            "Timestamp": timestamp, 
            "Application": application, 
            "Image": image, 
            "Container": container,
            "ExecutionTime": executionTime,
            "Coldstart": coldstart,
            "Runtime": runtime
        }

        await producer.produce_message(info)
        
    }


    
   
}

module.exports = new executeControllers() 