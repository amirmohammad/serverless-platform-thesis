// 3rd party dependencies

// local dependencies
const controller = require('./index.controllers')
const ImageSchema = require('../models/Image')
const Docker = require('dockerode');
const logger = require('../utils/logger');
const docker = new Docker(); 

class deleteController extends controller {

    async deleteFunction(req, res, next) {
        const {id} = req.params;

        return res.json(`delete function ${id}`)
    }

    async deleteImage(req, res, next) {
        if(! await this.validationData(req, res)) return;
        const {id} = req.params; 

        try {
            const image = await ImageSchema.findOne({ImageID: id}); 
            if(! image) {
                return this.responseMesaage(req, res, 404, 'Failed', 'delete Image', '', '/delete/image', 'Image not found')
            }
            try {
                return Promise.all([
                    docker.getImage(image.Name).remove(),
                    ImageSchema.deleteOne({ImageID: id})
                ])
                .then(this.responseMesaage(req, res, 200, 'Success', 'delete Image', '', '/delete/image', 'Image Deleted Successfully'))
            } catch (error) {
                logger.error(error)
                return this.responseMesaage(req, res, 500, 'Failed', 'delete Image', '', '/delete/image', 'Server-Side Error for deleting image with dockerode, Or image not exists')
            }
            
        } catch (error) {
            return this.responseMesaage(req, res, 500, 'Failed', 'delete Image', '', '/delete/image', 'Server-Side Error')
            
        }

        
    }

}

module.exports = new deleteController()
