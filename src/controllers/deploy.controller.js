// import 3rd party packages
const Docker  = require('dockerode')
const {promisify} = require('util')
const exec = promisify(require('child_process').exec);
const uniqueString = require('unique-string'); 

// import existing files
const controller = require('./index.controllers');
const ImageSchema = require('../models/Image');
const {AllInvocedFunctionsTransform} = require('../Transforms/deployTransform');
const logger = require('../utils/logger');

class deployControllers extends controller {

    //pulling image
    async pull(req, res, next) {
        if(! await this.validationData(req, res)) return;

        let {application, DockerImage} = req.body; 

        const functionId = uniqueString(); 

        if (!DockerImage.includes(':')) {
            DockerImage += ":latest"
        }

        // const image = await ImageSchema.findOne({ "Name": DockerImage}); 
        // return res.json(image)

        try {
            
            const docker = new Docker()
            const images = await docker.listImages();
            
            for(let image of images) {
                // if image is present in the list of images in the host, return success message and end the operation
                // if image exists, its informations is present in database
                if (image.RepoTags == DockerImage) {
                    // check if image exists in database 
                    const imageExists = await this.checkDBForImage(DockerImage) 

                    if (! imageExists) {
                        //create image
                        const newImage = new ImageSchema ({
                            Name: DockerImage, 
                            Application: application
                        })
                        image = await newImage.save()

                        return this.responseMesaage(req, res, 200, 'Success', DockerImage, `Docker pull ${DockerImage}`, 'deploy', 'Image added to database');
                    }                   
                    else 
                        return this.responseMesaage(req, res, 200, 'Success', DockerImage, `Docker pull ${DockerImage}`, 'deploy', 'Image Found Successfully');
                }
            }

            try {

                const pullImage = await docker.pull(DockerImage)
                // pull image and create database, if Exists just pull image ... 
                if (pullImage.statusCode == '200') {
                    let image = await ImageSchema.findOne({ 'Name': DockerImage});
                    if(!image) {
                        const newImage = new ImageSchema ({
                            Name: DockerImage, 
                            Application: application
                        })
                        image = await newImage.save()
                        if(!image) {
                            return this.responseMesaage(req, res, 500, 'Failed', DockerImage, `mongodb save ${DockerImage}`, 'deploy', 'Database client failed to save image')
                        }
                    }
                    // image Exists
                    const data = {
                        ImageID: image.ImageID,
                    }
                    return this.responseMesaageWithData(req, res, 200, 'Success', DockerImage, `Docker pull ${DockerImage}`, 'deploy', 'Image pulled Successfully', data);
                }
            } catch (error) {
                logger.error(error)
                //error in pulling image, maybe image not found or docker login required
                return this.responseMesaage(req, res, 404, 'Failed', DockerImage, `Docker pull ${DockerImage}`, 'deploy', 'Image Not Found, or docker login required.')
            }

        } catch (error) {
            // if request gots error
            console.log(error)
            return this.responseMesaage(req, res, 500, 'Failed', DockerImage, '', 'deploy', 'Server-Side Error')
        }
    }

    async getAll(req, res, next) {
        try {
            const functions = await ImageSchema.find({}, 'ImageID Name');
            let sanatized_functions = await AllInvocedFunctionsTransform(functions);
            let result = {
                "Functions": sanatized_functions, 
                "length": sanatized_functions.length,
            }
            return this.responseMesaageWithData(req, res, 200, 'Success', 'All', 'Find all Images', '/deploy/functions', 'all functions found successfully', result);
        } catch (error) {
            consoel.log(error)
            return this.responseMesaage(req, res, 500, 'Failed', 'all images', '', '/deploy/functions', 'Server-Side Error')
        }
    }

    async checkImageExists(image) {
        try {
            const docker = new Docker()
            const images = await docker.listImages()

            for(let img of images) {
                // if image is present in the list of images in the host, return success message and end the operation
                // if image exists, its informations is present in database
                if (image.RepoTags == image) {
                    // image Found
                    return true;
                }
            }

            return false;

        } catch (error) {
            return this.responseMesaage(req, res, 500, 'Failed', image, '', 'deploy', `error while trying to find ${image}`)
            
        }
    }

    async pullImage (image) {
        try {
            const docker = new Docker()
            const pullImage = await docker.pull(image)
                // pull image and create database, if Exists just pull image ... 
                if (pullImage.statusCode == '200') {
                    return true
                }
        } catch (error) {
            logger.error(`error while trying to find ${image}`, error)
            return false
        }
    }
    
    async checkDBForImage(image) {
        try {
            const result = await ImageSchema.findOne({"Name": image})
            // console.log(result)
            if (!result) 
                return false
            else 
                return true;
            
        } catch (error) {
            return false
            // return this.responseMesaage(req, res, 500, 'Failed', image, '', 'deploy', `error while trying to find ${image} in db`)
            
        }
    }

    async saveImageToDB(image) {
        const newImage = new ImageSchem({
            ImageID,
            Runtime,
            Name, 
        })
    }
    
}

module.exports = new deployControllers() 