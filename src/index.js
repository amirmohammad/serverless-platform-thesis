// Global Dependencies 
const express = require('express'); 
const methodOverride = require('method-override');
const bodyParser = require('body-parser'); 
const morgan = require('morgan')
const http = require('http')
const path = require('path');
const mongoose = require('mongoose');
const logger = require('./utils/logger');

// Local Dependencies


// Configurations
const app = express()

class cloudsecWorker {

    constructor() {
        // console.log('app initialized')
        this.setupExpress()
        this.setupDB();
        this.setupConfig()
        this.routes()
    }

    async setupExpress() {
        const server = await http.createServer(app)
        server.listen(config.port, () => logger.info(`${config.appName} listening to ${config.port}`))
    }

    async setupDB() {
        // console.log(global.config.db.port)
        mongoose.connect(`mongodb://${global.config.db.uri}:${global.config.db.port}/${global.config.db.name}`,{
            useNewUrlParser: true,
            useUnifiedTopology: true
            })
            .then(() => logger.info('connected successfully!'))
            .catch(err => {
                logger.error(err)
                process.exit(0)
            })
    }

    async setupConfig() {
        app.use(bodyParser.json()); 
        app.use(bodyParser.urlencoded({extended: true})); 

        app.use(methodOverride('_method'))
    }

    routes() {
        app.use('/', require('./routes/index.routes'));
    }


}

module.exports = cloudsecWorker; 