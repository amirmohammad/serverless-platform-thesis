const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

const validator = require('./index.validator')

class executeValidator extends validator {

    execute(){
        return [
            check('func')
                .trim()
                .notEmpty()
                .isString()
                .not().isNumeric()
                .not().isBoolean()
                .not().isEmail()
                .withMessage('Function Name should be specified as string'),
                
            check('user')
                .trim()
                .notEmpty()
                .isString()
                .not().isNumeric()
                .not().isBoolean()
                .not().isEmail()
                .withMessage('Username should be specified as string'),

            body('runtime')
                .trim()
                .notEmpty()
                .isString()
                .not().isNumeric()
                .not().isBoolean()
                .not().isEmail()
                .withMessage('runtime is invalid')
        ]
    }

}

module.exports = new executeValidator(); 