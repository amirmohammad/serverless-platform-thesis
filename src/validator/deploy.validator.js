const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

const validator = require('./index.validator')

class deployValidator extends validator {

    deploy(){
        return [
            check('DockerImage')
                .trim()
                .notEmpty()
                .isString()
                .not().isNumeric()
                .not().isBoolean()
                .not().isEmail()
                .withMessage('Image Name should be specified as string'),
            
            check('runtime')
                .trim()
                .notEmpty()
                .isString()
                .withMessage('runtime should be specified as string')
                .isIn(['gvisor', 'unik', 'unikraft', 'kata', 'docker', 'firecracker', 'lightvm'])
                .withMessage('runtime input is not valid'),
            
            check('application')
                .trim()
                .notEmpty()
                .isString()
                .withMessage('application should be specified as string')
                
                
        ]
    }

}

module.exports = new deployValidator(); 