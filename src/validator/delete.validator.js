const { check, body, sanitizeBody, buildCheckFunction, query, param } = require('express-validator');

const validator = require('./index.validator')

class deleteValidator extends validator {
    
    deleteImage() {
        return [
            query('id')
                .trim()
                .notEmpty()
                .withMessage('required parameter is empty')
        ]
    }
}

module.exports = new deleteValidator()