
const express = require('express')
const methodOverride = require('method-override');

const metricController = require('../controllers/metrics.controllers')
const deployController = require('../controllers/deploy.controller')
const executeController = require('../controllers/execute.controller')
const deleteController = require('../controllers/delete.controller')
const deployValidator = require('../validator/deploy.validator')
const deleteValidator = require('../validator/delete.validator')
const executeValidator = require('../validator/execute.validator')

const router = express.Router();

// metric routes
router.get('/metrics', (req, res, netxt) => {
    metricController.showMetrics(req, res); 
})

// execute routes
router.post('/execute/:user/:func', executeValidator.execute(), (req, res, next ) => {
    executeController.execute(req, res);
})

// deploy routes
router.post('/deploy', deployValidator.deploy(), (req, res, next) => {
    deployController.pull(req, res); 
})
router.get('/deploy/functions', (req, res, next) => {
    deployController.getAll(req, res); 
})

// remove routes
router.delete('/delete/function/:id', (req, res, next) => {
    deleteController.deleteFunction(req, res);
})
router.delete('/delete/image/:id', deleteValidator.deleteImage(), (req, res, next) => {
    deleteController.deleteImage(req, res);
})

router.post('/test', (req, res) => res.json(req.body))


module.exports = router; 