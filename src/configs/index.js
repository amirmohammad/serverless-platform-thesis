const db = require('./db')
const docker = require('./docker'); 
const host = require('./host')
const kafka = require('./kafka')

module.exports = {
    port: process.env.PORT || 3030, 
    appName: 'cloudsec',
    db, 
    docker,
    host,
    kafka, 
}