
module.exports = {
    "bridge": process.env.DOCKER_NETWORK_BRIDGE,
    "host": process.env.DOCKER_NETWORK_HOST,
    "containerWaitOptimalDuration": process.env.CONTAINER_WAIT_OPTIMAL_DURATION,
    "memory": "128mb", 
}