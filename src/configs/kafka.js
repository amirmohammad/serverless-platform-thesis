module.exports = {
    "url": process.env.KAFKA_URI || 'localhost',
    "port": process.env.KAFKA_PORT || 9092,
    "clientId": process.env.KAFKA_CLIENT_ID,
    "topics": {
        "log_channel": "log_channel",
        "container": "container",
        "gvisor":"gvisor",
        "autoscale": "autoscale",
        "preload": "preload",
        "metric": "metric"
    }
}