
module.exports = {
    "url": process.env.HOST_URL,
    "communicationProtocol": process.env.COMMUNICATION_PROTOCOL,
}