
module.exports = {
    uri: process.env.DB_URL || 'localhost', 
    port: process.env.DB_PORT || '27017',
    name: process.env.DB_NAME || 'worker',
}