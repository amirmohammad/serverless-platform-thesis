from pydoc_data.topics import topics
from kafka.admin import KafkaAdminClient, NewTopic
from kafka import KafkaConsumer

admin_client = KafkaAdminClient(
    bootstrap_servers="localhost:9092", 
    client_id='test'
)

consumer = KafkaConsumer(  bootstrap_servers="localhost:9092", 
                client_id='test'
            )

print(f"All Topics {consumer.topics()}")

from dotenv import load_dotenv
load_dotenv()
import os 
import json




# topic_list = []
# topic_list.append(NewTopic(name="topic03", num_partitions=1, replication_factor=1))
# # print(topic_list)
# admin_client.create_topics(new_topics=topic_list, validate_only=False)

# #Delete All Topics
# deleteTopics = admin_client.delete_topics(list(consumer.topics()))

def list_all_topics():
    return consumer.topics()


def add_topics(topics_to_add):
    formatted_topics = []

    for topic in topics_to_add:
        formatted_topic = NewTopic(name=topic, num_partitions=1, replication_factor=1)
        formatted_topics.append(formatted_topic)

    admin_client.create_topics(formatted_topics, validate_only=False)    


def delete_all_topics():
    return admin_client.delete_topics(list(consumer.topics()))


if __name__ == '__main__':

    deleteAllTopicsResult = delete_all_topics()

    pre_activating_topics = list_all_topics();
    print(f"List of all topics before adding to kafka: {pre_activating_topics}")

    topic_files = open('topics.json')
    topic_files = json.load(topic_files)

    topics_to_add=[]
    for topic in topic_files:
        topics_to_add.append(topic)
    print(f"Topics to add: {topics_to_add}")

    addTopicsResult = add_topics(topics_to_add)

    print(f"list of topics at the end: {list_all_topics()}")