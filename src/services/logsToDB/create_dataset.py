import docker
client = docker.from_env()


def create_data_format(info):
    # return 'hello'
    container = client.containers.get(info['Container'])
    status = container.stats(stream=False)

    totalCPUUsage = status['cpu_stats']['cpu_usage']['total_usage']
    numberOfCPUs = status['cpu_stats']['online_cpus']
    memoryLimit = status['memory_stats']['limit']
    memoryUsage = status['memory_stats']['usage']
    timestamp = info['Timestamp']
    executionTime = info['ExecutionTime']
    image = info['Image']
    coldstart = info['Coldstart']
    container = info['Container']
    application = info['Application']
    runtime = info['Runtime']

    return timestamp, image, container, coldstart, numberOfCPUs, totalCPUUsage, memoryUsage, memoryLimit, executionTime, application, runtime


# if __name__ == '__main__':
#     create_data_format(info)