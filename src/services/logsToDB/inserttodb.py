from pymongo import MongoClient

def insert_mongo(timestamp, image, container, coldstart, numberOfCPUs, totalCPUUsage, memoryUsage, memoryLimit, executionTime, application, runtime):
    client = MongoClient('127.0.0.1', 27017)
    db = client['cloudsec-worker']
    data = db.data

    dataObject = {
        'timestamp': timestamp,
        'image': image,
        'container': container, 
        'coldstart': coldstart,
        'numberOfCPUs': numberOfCPUs,
        'totalCPUUsage': totalCPUUsage,
        'memoryUsage': memoryUsage, 
        'memoryLimit': memoryLimit,
        'executionTime': executionTime,
        'application': application,
        'runtime': runtime,
        'active': True,
    }

    data.insert_one(dataObject)

# if __name__ == '__main__':
#     insert_mongo(timestamp, image, container, coldstart, numberOfCPUs, totalCPUUsage, memoryUsage, memoryLimit, executionTime, application, runtime)






