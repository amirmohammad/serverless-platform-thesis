const { Kafka, Partitioners, CompressionTypes } = require('kafkajs');
const logger = require('../../utils/logger');

const topic = global.config.kafka.topics.log_channel; 

const connectKafka = async () =>{
    let kafkaURI = `${global.config.kafka.url}:${global.config.kafka.port}`

    const kafka = new Kafka({
        brokers: [kafkaURI]
    })

    const producer = kafka.producer({createPartitioner: Partitioners.DefaultPartitioner });

    await producer.connect()

    return producer;
}

const sendMessage = async (producer, info) => {

    let data = {
        "key": info.container,
        "value": JSON.stringify(info)
    }

    return producer.send({
        topic: global.config.kafka.topics.log_channel, 
        compression: CompressionTypes.GZIP,
        messages: [data]
    })
    .then(logger.info)
    .catch(e => logger.error(e.message))
}

const produce_message = async (info) => {
    let producer = await connectKafka();
    let result = await sendMessage(producer, info)
}

module.exports = {
    produce_message, 
}