#!/usr/bin/python3

from pymongo import MongoClient
from dotenv import dotenv_values
from datetime import datetime, timedelta
import docker
import logging

config = dotenv_values('/home/amirmohammad/Documents/code/serverless_thesis/src/services/zero_scale/.env')
# client = MongoClient(config['DB_URL'], int(config['DB_PORT']))
client = MongoClient("127.0.0.1", 27017)
db = client['cloudsec-worker']
data_collection = db.data
docker_clinet = docker.from_env()
logging.basicConfig(    filename='/home/amirmohammad/Documents/code/serverless_thesis/src/services/zero_scale/zeroscaler.log', 
                        filemode='a', level=logging.INFO, 
                        format='%(asctime)s - %(levelname)s - %(message)s'
                    )

def check_if_timestamp_expired(timestamp):
    # warm_duration_in_minutes = int(config['ZEROSCALING_TIME'])
    warm_duration_in_minutes = 1
    now = datetime.now()
    then = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
    
    added_time = then + timedelta(minutes=warm_duration_in_minutes)

    if (now >= added_time): 
        return True
    return False

def remove_container(container_id):
    try:
        container = docker_clinet.containers.get(container_id=container_id)
        removed = container.remove(force=True)
        return removed
    except Exception as err:
        logging.error(f"error: {err}")
        return False

def zero_scaling():
    functions = data_collection.find({}, {"container": 1})
    
    function_ids = set()
    for function in functions:
        container_id = function['container']
        function_ids.add(container_id)
    function_ids = list(function_ids)

    for function_id in function_ids:
        
        function = data_collection.find({  "container": function_id }, {}).sort("_id", -1)
        function_timestamp = function[0]['timestamp']
        
        if(function[0]['active'] == False):
            continue
        
        is_expired = check_if_timestamp_expired(function_timestamp)

        if (is_expired == True): 
            #container is expired, it should be removed
            remove = remove_container(function_id)
            if (remove != False):
                logging.info(f"Container {function_id} scaled to Zero.")
                #update database
                update_function = data_collection.find_one_and_update(
                                                                        {'_id': function[0]['_id']},
                                                                        {'$set': { "active" : False }}
                                                                    )
            

    logging.info('Application Finished!')
    #close pymongo connection
    client.close()

zero_scaling()
