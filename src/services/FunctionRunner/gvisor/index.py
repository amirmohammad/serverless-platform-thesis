from kafka import KafkaConsumer, consumer
import json
from docker_gvisor import run_container
from producer import send_message

class MessageConsumer:
    broker = ""
    topic = ""
    group_id = ""
    logger = None

    def __init__(self, broker, topic):
        self.broker = broker
        self.topic = topic

    def activate_listener(self):
        # consumer = KafkaConsumer(bootstrap_servers=self.broker,
        #                          consumer_timeout_ms=60000,
        #                          auto_offset_reset='earliest',
        #                          enable_auto_commit=False,
        #                          value_deserializer=lambda m: json.loads(m.decode('ascii')))

        consumer = KafkaConsumer(
            self.topic, auto_offset_reset='latest')

        # consumer.subscribe(self.topic)
        print("consumer is listening....")
        try:
            for message in consumer:
                image = message.value
                containerId, containerPort = run_container(image)
                data = {
                    "containerId": containerId, 
                    "containerPort": containerPort
                }
                
                send_message(str(data))



        except KeyboardInterrupt:
            print("Aborted by user...")
        finally:
            consumer.close()


#Running multiple consumers
broker = 'localhost:9092'
topic = 'gvisor'
group_id = 'consumer-1'

consumer1 = MessageConsumer(broker,topic)
consumer1.activate_listener()
