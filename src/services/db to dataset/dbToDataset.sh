#!/bin/bash

mongoexport \
    --uri="mongodb://127.0.0.1:27017/cloudsec-worker" \
    --collection=data  \
    --csv \
    --out=dataset.csv \
    --fields \
        _id,timestamp,image,container,coldstart,numberOfCPUs,totalCPUUsage,memoryUsage,memoryLimit,executionTime