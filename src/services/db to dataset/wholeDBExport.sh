#!/bin/bash

mongodump \
    --uri="mongodb://127.0.0.1:27017/cloudsec-worker" \
    --out=db \
    